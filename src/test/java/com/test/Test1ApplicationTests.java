package com.test;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.test.controller.TestController;
import com.test.controller.handler.RestExceptionHandler;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Test1ApplicationTests {

	private MockMvc mockMvc;

	@InjectMocks
	private TestController testController;

	@Before
	public void init() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(testController)
//                .addFilters(new CORSFilter())
				.setControllerAdvice(new RestExceptionHandler()).build();

	}

	@Test
	public void testDefaultUrl() throws Exception {
		MvcResult result = mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk()).andReturn();
		
		String content = result.getResponse().getContentAsString();
		
		assertTrue(content.equals("HELLO WORLD"));
	}

	@Test
	public void testHellWorldUrl() throws Exception {
		MvcResult result = mockMvc.perform(get("/helloWorld")).andDo(print()).andExpect(status().isOk()).andReturn();
		
		String content = result.getResponse().getContentAsString();
		
		assertTrue(content.equals("HELLO WORLD@@"));
	}
	
	@Test
	public void testTestUrl() throws Exception {
		MvcResult result = mockMvc.perform(get("/test")).andDo(print()).andExpect(status().isOk()).andReturn();
		
		String content = result.getResponse().getContentAsString();
		
		assertTrue(content.equals("test!!"));
	}	
}
