package com.test.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class TestController {

	@RequestMapping("/")
	public String test() {
		return "HELLO WORLD";
	}
	
	@RequestMapping("/helloWorld")
	public String test2() {
		return "HELLO WORLD@@";
	}
	
	@RequestMapping("/test")
	public String test3() {
		return "test!!";
	}	
	
	
}
