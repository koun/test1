FROM java:8
VOLUME /tmp
ADD target/test1-1.0.0#*.jar ./
ENTRYPOINT exec java $JAVA_OPTS -D -jar *.jar 2>./log/test.stderr.log